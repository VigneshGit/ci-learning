FROM openjdk:8
EXPOSE 8080
ADD /build/libs/spring-boot-in-docker.jar greetings.jar
ENTRYPOINT ["java","-jar","greetings.jar"]

# In add we provided the destination file as spring-boot-in-docker.jar, same should be the ENTRYPOINT

#FROM = pulling an image from the Public Repositories & occurs multiple times.
        # Syntax: FROM [--platform=<platform>] <image> [AS <name>]
        # In above, openjdk is pulling from Public Repository

#EXPOSE = container listens on the specified network ports at runtime.
          # Syntax: EXPOSE <port> [<port>/<protocol>...]
          # In above, container listens at the port 8081

#ADD = Add the fileSystem of the image at the path dest.
       # Syntax: ADD [--chown=<user>:<group>] <src>... <dest>
       # The --chown feature is only supported on Dockerfiles used to build Linux containers, and will not work on Windows containers.
       # In above, <src> as "/build/libs/spring-boot-in-docker.jar" & <dest> as "dockerWorkDir"

#ENTRYPOINT = Allows you to configure a container that will run as an executable.
              # Syntax: ENTRYPOINT ["executable", "param1", "param2"]
              # In above, Execute the docker run cmd, will be appended after all elements in an exec form ENTRYPOINT

#RUN =  It will execute any commands in a new layer on top of the current image and commit the results.
        # The resulting committed image will be used for the next step in the Dockerfile.
        # Syntax: RUN ["executable", "param1", "param2"] (exec form)


